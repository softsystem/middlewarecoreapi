using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace middlewareApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            /////////////////////////// Understanding the Run, Use, and Map Method ////////////////////////////////////////////
            /// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // app.Use(async (context, next) =>
            // {
            //     await context.Response.WriteAsync("Before Invoke from 1st app.Use()\n");
            //     await next();
            //     await context.Response.WriteAsync("After Invoke from 1st app.Use()\n");
            // });

            // app.Use(async (context, next) =>
            // {
            //     await context.Response.WriteAsync("Before Invoke from 2nd app.Use()\n");
            //     await next();
            //     await context.Response.WriteAsync("After Invoke from 2nd app.Use()\n");
            // });

            // app.Run(async (context) =>
            // {
            //     await context.Response.WriteAsync("Hello from 1st app.Run()\n");
            // });

            // // the following will never be executed
            // app.Run(async (context) =>
            // {
            //     await context.Response.WriteAsync("Hello from 2nd app.Run()\n");
            // });           
            //////////////////////////////////////Custom Middleware////////////////////////////////////////////////////////////////////////////////
            //app.UseLogUrl();

            /// <summary>
            /// Enabling directory browsing through Middleware
            /// </summary>
            /// <value></value>
            app.UseDirectoryBrowser(new DirectoryBrowserOptions
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "images")),
                RequestPath = "/images"
            });


            app.UseHttpsRedirection();

            ////////////////////////////////////////////////////app.Map() Method//////////////////////////////////////////////////////////
            /////////
            ///

            // app.Map("/m1", HandleMapOne);
            // app.Map("/m2", appMap =>
            // {
            //     appMap.Run(async context =>
            //     {
            //         await context.Response.WriteAsync("Hello from 2nd app.Map() Method M2");
            //     });
            // });
            // app.Run(async (context) =>
            // {
            //     await context.Response.WriteAsync("Hello from app.Run() default path");
            // });

            

            /////////////////////////////////////////////////////////////////////////////////////////////


        }

        private static void HandleMapOne(IApplicationBuilder app)
        {
            app.Run(async context =>
            {
                await context.Response.WriteAsync("Hello from Handle Map One path M 1");
            });
        }
    }
}
